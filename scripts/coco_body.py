CocoBodyPlan = {
    "names": {
        0: "Nose",
        1: "Neck",
        2: "RShoulder",
        3: "RElbow",
        4: "RWrist",
        5: "LShoulder",
        6: "LElbow",
        7: "LWrist",
        8: "RHip",
        9: "RKnee",
        10: "RAnkle",
        11: "LHip",
        12: "LKnee",
        13: "LAnkle",
        14: "REye",
        15: "LEye",
        16: "REar",
        17: "LEar",
        18: "Bkg"},
    "links": [[1, 2], [1, 5], [2, 3], [3, 4], [5, 6], [6, 7], [1, 8], [8, 9], [9, 10], [1, 11], [11, 12],
        [12, 13], [1, 0], [0, 14], [14, 16], [0, 15], [15, 17]]}

CocoBodyPlan['ids'] = dict((v,k) for k,v in CocoBodyPlan['names'].iteritems())

